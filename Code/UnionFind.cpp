#include "UnionFind.hpp"
#include <iostream>

UnionFind::UnionFind(std::vector<int> nParent)
{
    std::copy(nParent.begin(), nParent.end(), std::back_inserter(parent));
    
}

void UnionFind::affichage()
{
    std::cout<<'[';
    for(int i = 0; i < parent.size(); i++)
    {
        std::cout<<parent[i];
        if(i != parent.size() - 1)
        {
            std::cout<<", ";
        }
    }
    std::cout<<"]"<<std::endl;
    
}

int UnionFind::Find(int v)
{
    int temp = v;
    while(parent[v] != v)
    {
        v = parent[v];
        parent[temp] = v;
    }
    return v;
}

void UnionFind::UnionWithRacine(int r1, int r2) // R1 --> R2
{

    if(hauteur[r1] > hauteur[r2]) //2.42 2.52
    {
        hauteur[r2]++;
        parent[r2] = r1;
    }
    else
    {
        hauteur[r1]++;
        parent[r1] = r2;
    }
}

void UnionFind::Union(int r1, int r2)
{
    UnionWithRacine(Find(r1), Find(r2));
}

UnionFind::~UnionFind()
{

}