#ifndef UNIONFIND_HPP
#define UNIONFIND_HPP

#include <vector>
class UnionFind
{
private:
    
public:
    std::vector<int> parent;
    std::vector<int> hauteur;
    UnionFind(std::vector<int> nParent);
    ~UnionFind();
    int Find(int v);
    void Union(int r1, int r2);
    void UnionWithRacine(int r1, int r2);
    void affichage();
};



#endif //UNIONFIND_HPP